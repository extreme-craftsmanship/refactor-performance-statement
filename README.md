# Refactor @refactor-performance-statement

## 开发环境
- JDK11+

## 业务目标
这是来自《重构》第二版中的第一个案例：
设想有一个戏剧演出团，演员们经常要去各种场合表演戏剧。通常客户 (customer)会指定几出剧目，而剧团则根据观众(audience)人数及剧目类型来 向客户收费。
该团目前出演两种戏剧:悲剧(tragedy)和喜剧(comedy)。
给客户发出账单时，剧团还会根据到场观众的数量给出“观众量积分”(volume credit)优惠，下次客户再请剧团表演时可以使用积分获得折扣——你可以把它看 作一种提升客户忠诚度的方式。
该剧团将剧目的数据存储在一个简单的JSON文件中。

## 题目要求
1. 小步提交，说明重构意图

## 重构步骤
1. 构建保护测试网
2. 识别坏味道
3. 旧的不变，新的创建，一步替换，旧的再见
4. 运行测试
5. 2-4♻️

## 参考资料
- [JUnit 5用户指南](https://gitee.com/liushide/junit5_cn_doc/blob/master/junit5UserGuide_zh_cn.md#https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fjunit-team%2Fjunit5-samples%2Ftree%2Fr5.0.2%2Fjunit5-gradle-consumer)
- [Gradle 用户指南](https://docs.gradle.org/current/userguide/userguide.html)